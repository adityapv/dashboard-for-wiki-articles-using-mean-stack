'use strict';
var path = require('path');
var mongoose = require('mongoose'),
  Revisions = mongoose.model('Revisions');

exports.list_all_tasks = function(req, res) {
  Revisions.aggregate(([{$group:{_id:'$title',numOfEdits:{$sum:1}}},{$sort:{numOfEdits:-1}},{$limit:1}]), function(err, result) {
	console.log(result);	
	console.log('get all tasks'); 
    if (err)
      res.send(err);
    res.json(result);
  });
};

//used
exports.get_all_articles_title = function(req, res) {
   Revisions.distinct('title', function(err, titles) {
   if (err)
		res.send(err);
	res.json(titles);
   });
};

exports.create_a_task = function(req, res) {
  var revisions = new Revisions(req.body);
  revisions.save(function(err, revisions) {
    if (err)
      res.send(err);
    res.json(revisions);
  });
};

//used
exports.get_article_details = function(req, res) {
  Revisions.find({'title': req.params.title}).count(function(err, revisions) {
    if (err)
      res.send(err);
    res.json(revisions);
  });
};

//top 5 registered editors for articles
exports.get_top5_editors_for_artcles = function(req, res) {
  console.log(req.params.title);	
  Revisions.aggregate(([{'$match':{$and:[{'anon':{$ne:''}},{'title': req.params.title}]}},{$group:{_id:'$user',numOfEdits:{$sum:1}}},{$sort:{numOfEdits:-1}},{$limit:5}]),function(err, top5users) {
    if (err)
      res.send(err);
    res.json(top5users);
  });
};

exports.get_revisions_by_year = function(req, res) {
  console.log(req.params.titleAndUser);
  console.log(users);
  var token = req.params.titleAndUser.split('~');
  db.getCollection('revisions').aggregate(([{'$match': {$and: [{$or:[{user:+token[1]}, {user:+token[2]}, {user:+token[3]}, {user:+token[4]}, {user:+token[5]}]} ,{title: +token[0]}]}},
		{$group:{_id:{$substr:[ '$timestamp', 0, 4 ]},numOfEdits:{$sum:1}}},{$sort:{_id:1}}]),function(err, top5users) {
    if (err)
      res.send(err);
    res.json(top5users);
  });
};


exports.update_a_task = function(req, res) {
  Revisions.findOneAndUpdate(req.params.taskId, req.body, {new: true}, function(err, revisions) {
    if (err)
      res.send(err);
    res.json(revisions);
  });
};
// Task.remove({}).exec(function(){});
exports.delete_a_task = function(req, res) {

  Revisions.remove({
    _id: req.params.taskId
  }, function(err, revisions) {
    if (err)
      res.send(err);
    res.json({ message: 'revisions successfully deleted' });
  });
};

exports.get_article_with_most_revisions = function(req, res) {
	  Revisions.aggregate(([{$group:{_id:'$title',numOfEdits:{$sum:1}}},{$sort:{numOfEdits:-1}},{$limit:1}]), function(err, revisions) {
	    if (err)
	      res.send(err);
	    res.json(revisions);
	  });
	};	

exports.get_article_with_least_revisions = function(req, res) {
		  Revisions.aggregate(([{$group:{_id:'$title',numOfEdits:{$sum:1}}},{$sort:{numOfEdits:1}},{$limit:1}]), function(err, revisions) {
		    if (err)
		      res.send(err);
		    res.json(revisions);
		  });
		};
		
exports.get_article_with_highest_history = function(req, res) {
  Revisions.aggregate(([{$group:{_id:'$title',time1:{$min:'$timestamp'}}},{$sort:{time1:1}},{$limit:1}]), function(err, revisions) {
    if (err)
      res.send(err);
    res.json(revisions);
  });
};		
			
exports.get_article_with_lowest_history = function(req, res) {
  Revisions.aggregate(([{$group:{_id:'$title',time2:{$min:'$timestamp'}}},{$sort:{time2:-1}},{$limit:1}]), function(err, revisions) {
    if (err)
      res.send(err);
    res.json(revisions);
  });
};	

/**exports.get_article_with_largest_group = function(callback){
    return this.aggregate({$match:  {$and: [{embeddedData:{$exists: true, $size:0} }, {anon:{$exists:false}}]}},
        {
            $group : {_id : "$title", count3 : { $sum : 1 }}
        },
        {
            $sort : {count3 : -1}
        }
    )
        .limit(1)
        .exec(callback)
} **/

exports.get_article_with_largest_group = function(req, res) {
	 Revisions.aggregate(([  {$match:  {$and: [{embeddedData:{$exists: true, $size:0} }, {anon:{$exists:false}}]}},
             {
                 $group : {_id : "$title", count3 : { $sum : 1 }}
             },
             {
                 $sort : {count3 : -1}
             },
             {$limit:1}]
     ), function(err, revisions) {
	    if (err)
	      res.send(err);
	    res.json(revisions);
	  });
	};	
	
exports.get_article_with_smallest_group = function(req, res) {
	Revisions.aggregate(([  {$match:  {$and: [{embeddedData:{$exists: true, $size:0} }, {anon:{$exists:false}}]}},
            {
                $group : {_id : "$title", count4 : { $sum : 1 }}
            },
            {
                $sort : {count4 : 1}
            },
            {$limit:1}]
    ), function(err, revisions) {
	    if (err)
	      res.send(err);
	    res.json(revisions);
	  });
	};

exports.PieAnon = function(req, res) {
    Revisions.aggregate(([
        {$match:{anon:{$exists:true}}},
            {
                $group : {_id : "$anon", countPA : { $sum : 1 }}
            },
            {
                $sort : {countPA :1}
            },{$limit:1}]
    ), function(err, revisions) {
        if (err)
            res.send(err);
        res.json(revisions);
    });
};

exports.IndPieAnon = function(req, res) {
    Revisions.aggregate(([
            {$match:{$and:[{'title':req.params.title},{anon:{$exists:true}}]}},
            {
                $group : {_id : "$anon", countIPA : { $sum : 1 }}
            },
            {
                $sort : {countIPA :1}
            },{$limit:1}]
    ), function(err, revisions) {
        if (err)
            res.send(err);
        res.json(revisions);
    });
};

exports.PieBot = function(req, res) {
    Revisions.aggregate(([
        {$match:  {$and: [{embeddedData:{$exists: true, $size:1} }, {embeddedData:{$elemMatch:{botflag:"1"}}}]}},
            {
                $group : {_id : "embeddedData", countBot : { $sum : 1 }}
            },
            {
                $sort :{countBot:1}
            },{$limit:1}]
    ), function(err, revisions) {
        if (err)
            res.send(err);
        res.json(revisions);
    });
};

exports.IndPieBot = function(req, res) {
    Revisions.aggregate(([
            {$match:  {$and: [{'title':req.params.title},{embeddedData:{$exists: true, $size:1} }, {embeddedData:{$elemMatch:{botflag:"1"}}}]}},
            {
                $group : {_id : "embeddedData", countIBot : { $sum : 1 }}
            },
            {
                $sort :{countIBot:1}
            },{$limit:1}]
    ), function(err, revisions) {
        if (err)
            res.send(err);
        res.json(revisions);
    });
};

/*exports.PieAdmin = function(req, res) {
    Revisions.aggregate(([
        {$match:  {$and: [{embeddedData:{$exists: true, $size:1} }, {embeddedData:{$elemMatch:{botflag:"0"}}}]}},
            {
                $group : {_id : "embeddedData", countAdmin : { $sum : 1 }}
            },
            {
                $sort :{countAdmin:1}
            },{$limit:1}]
    ), function(err, revisions) {
        if (err)
            res.send(err);
        res.json(revisions);
    });
};*/


exports.PieAdmin = function(req, res) {
    Revisions.aggregate(([
            {$match:  {$and: [{embeddedData:{$exists: true, $size:1} }, {embeddedData:{$elemMatch:{botflag:"0"}}}]}},
            {
                $group : {_id : "embeddedData", countAdmin : { $sum : 1 }}
            },
            {
                $sort :{countAdmin:1}
            },{$limit:1}]
    ), function(err, revisions) {
        if (err)
            res.send(err);
        res.json(revisions);
    });
};

exports.IndPieAdmin = function(req, res) {
    Revisions.aggregate(([
            {$match:  {$and: [{'title':req.params.title},{embeddedData:{$exists: true, $size:1} }, {embeddedData:{$elemMatch:{botflag:"0"}}}]}},
            {
                $group : {_id : "embeddedData", countIAdmin : { $sum : 1 }}
            },
            {
                $sort :{countIAdmin:1}
            },{$limit:1}]
    ), function(err, revisions) {
        if (err)
            res.send(err);
        res.json(revisions);
    });
};
exports.PieReg = function(req, res) {
    Revisions.aggregate(([
        {$match:  {$and: [{embeddedData:{$exists: true, $size:0} }, {anon:{$exists:false}}]}},
            {
                $group : {_id : "embeddedData", countPR : { $sum : 1 }}
            },
            {
                $sort :{countPR:1}
            },{$limit:1}]
    ), function(err, revisions) {
        if (err)
            res.send(err);
        res.json(revisions);
    });
};

exports.IndPieReg = function(req, res) {
    Revisions.aggregate(([
            {$match:  {$and: [{'title':req.params.title},{embeddedData:{$exists: true, $size:0} }, {anon:{$exists:false}}]}},
            {
                $group : {_id : "embeddedData", countIPR : { $sum : 1 }}
            },
            {
                $sort :{countIPR:1}
            },{$limit:1}]
    ), function(err, revisions) {
        if (err)
            res.send(err);
        res.json(revisions);
    });
};


exports.home = function(req, res) {
	Revisions.distinct('title', function(err, titles) {
   if (err)
		res.send(err);
	res.sendFile(path.resolve('index.html'));
   });
};
