'use strict';

module.exports = function(app) {
	var todoList = require('../controllers/todoListController');

	// todoList Routes
	app.route('/tasks')
		.get(todoList.list_all_tasks)
		.post(todoList.create_a_task);

	app.route('/tasks/:taskId')
		.put(todoList.update_a_task)
		.delete(todoList.delete_a_task);
	
	app.route('/getArticleWithMostRevisions').get(todoList.get_article_with_most_revisions);
	app.route('/getArticleWithLeastRevisions').get(todoList.get_article_with_least_revisions);
	app.route('/getArticleWithHighestHistory').get(todoList.get_article_with_highest_history);
	app.route('/getArticleWithLowestHistory').get(todoList.get_article_with_lowest_history);
	app.route('/getArticleWithLargestgroupReguser').get(todoList.get_article_with_largest_group);
	app.route('/getArticleWithSmallestgroupReguser').get(todoList.get_article_with_smallest_group);
	app.route('/getAllTitles').get(todoList.get_all_articles_title);
	app.route('/getArticleDetails/:title').get(todoList.get_article_details);
	app.route('/getTop5EditorsForArticles/:title').get(todoList.get_top5_editors_for_artcles);
	app.route('/getRevisionsByYear/:titleAndUsers').get(todoList.get_revisions_by_year);
    app.route('/PieBot').get(todoList.PieBot);
    app.route('/PieAnon').get(todoList.PieAnon);
    app.route('/PieAdmin').get(todoList.PieAdmin);
    app.route('/PieReg').get(todoList.PieReg);
    app.route('/IndPieBot/:title').get(todoList.IndPieBot);
    app.route('/IndPieAdmin/:title').get(todoList.IndPieAdmin);
    app.route('/IndPieReg/:title').get(todoList.IndPieReg);
    app.route('/IndPieAnon/:title').get(todoList.IndPieAnon);
	
	app.route('/').get(todoList.home);
};
