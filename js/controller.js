angular.module('Demo', ['CustomSelect'])
.controller('DemoController', function ($scope, $http, $timeout, $q) {

    var anon;
    var bot;
    var reg;
    var admin;
    var indanon;
    var indbot;
    var indreg;
    var indadmin;
	
	$http.get('http://localhost:3000/getAllTitles').
		then(function(response) {
		var range = [];	
		for(var i=1; i<=response.data.length; i++)	
		{
			range.push({ id: response.data[i-1], name: response.data[i-1] });
		}
		$scope.states = range;
	});
	
	$scope.radioChange = function (s) {
 			if(s == 'overAll'){
				$scope.overAllSelected = true;
				$scope.singleArticleSelected = false;
				$http.get('http://localhost:3000/getArticleWithMostRevisions/').
					then(function(response) {
					$scope.mostarticle = response.data[0];
				})
					$http.get('http://localhost:3000/getArticleWithLeastRevisions/').
					then(function(response) {
					$scope.leastarticle = response.data[0];
				}	
					)
					$http.get('http://localhost:3000/getArticleWithHighestHistory/').
					then(function(response) {
					$scope.highhistory = response.data[0];
				}	
					)
				$http.get('http://localhost:3000/getArticleWithLowestHistory/').
					then(function(response) {
					$scope.leasthistory = response.data[0];
				}	
					)


                $http.get('http://localhost:3000/PieAnon/').
                then(function(response) {

                        anon = response.data[0].countPA;
                        console.log(anon)
                    }
                )
                $http.get('http://localhost:3000/PieBot/').
                then(function(response) {

                        bot = response.data[0].countBot;
                        console.log(bot)
                    }
                )
                $http.get('http://localhost:3000/PieReg/').
                then(function(response) {

                        reg = response.data[0].countPR;
                        console.log(reg);
                    }
                )

                $http.get('http://localhost:3000/PieAdmin/').
                then(function(response) {

                        admin = response.data[0].countAdmin;
                        console.log(admin)
                    }
                )
					
				$http.get('http://localhost:3000/getArticleWithSmallestgroupReguser/').
					then(function(response) {
					$scope.smallestgroup = response.data[0];
				}	
					)
					
				$http.get('http://localhost:3000/getArticleWithLargestgroupReguser/').
					then(function(response) {
					$scope.largestgroup = response.data[0];
				}	
					)	
					;
                document.getElementById('canvas2').innerHTML = '<canvas id="doughnut-chart" width="300" />';
                new Chart(document.getElementById("doughnut-chart"), {
                    type: 'doughnut',
                    data: {
                        labels: ["Anononymous", "Bots", "Admin", "Regular"],
                        datasets: [
                            {
                                label: "Num  of revisions",
                                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9"],
                                data: [anon,bot,admin,reg]
                            }
                        ]
                    },
                    options: {
                        title: {
                            display: true,
                            text: 'Predicted world population (millions) in 2050',
                            responsive: false,
                            maintainAspectRatio: false
                        }
                    }
                });

			}
			if(s == 'singleArticle') {
				$scope.overAllSelected = false;
				$scope.singleArticleSelected = true;




			}
    };
		
	var userList = [];
	var titleAndUsers = '';	
	
	$scope.change = function(s) {
		//titleAndUsers = s.state+'~';
		$http.get('http://localhost:3000/getTop5EditorsForArticles/'+s.state).
			then(function(res) {
			$scope.users = res.data;
			userList = res.data;
		//	for (var i=0; userList.length; i++){
		//		titleAndUsers = titleAndUsers + '~' + userList[i];
		//	}
		});

        $http.get('http://localhost:3000/IndPieAnon/'+s.state).
        then(function(response) {

                indanon = response.data[0].countIPA;
                console.log(indanon)
            }
        );
        $http.get('http://localhost:3000/IndPieBot/'+s.state).
        then(function(response) {

                indbot = response.data[0].countIBot;
                console.log(indbot)
            }
        );
        $http.get('http://localhost:3000/IndPieReg/'+s.state).
        then(function(response) {

                indreg = response.data[0].countIPR;
                console.log(indreg);
            }
        );

        $http.get('http://localhost:3000/IndPieAdmin/'+s.state).
        then(function(response) {

                indadmin = response.data[0].countIAdmin;
                console.log(indadmin)
            }
        );
		
		$http.get('http://localhost:3000/getArticleDetails/'+s.state).
			then(function(response) {
			$scope.articleSelected = 'truthy';
			$scope.revisionCount = response.data;
		});
		
		$http.get('http://localhost:3000/getRevisionsByYear/'+s.titleAndUsers).
			then(function(response) {
			$scope.articleSelected = 'truthy';
			$scope.revisionCount = response.data;
		});
		
		
			document.getElementById('canvas1').innerHTML = '<canvas id="bar-chart1" style="width: content-box;" />';
			document.getElementById('canvas2').innerHTML = '<canvas id="doughnut-chart" width="300" />';
			document.getElementById('canvas3').innerHTML = '<canvas id="bar-chart2" style="width: content-box;" />';
            document.getElementById('canvas4').innerHTML = '<canvas id="pie-chart" width="300" />';
						
			new Chart(document.getElementById("bar-chart1"), {
				type: 'bar',
				data: {
				  labels: ["Anononymous", "Bots", "Admin", "Regular"],
				  datasets: [
					{
					  label: "Number of revisions for overall data for different user types)",
					  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
					  data: [anon,bot,admin,reg]
					}
				  ]
				},
				options: {
				  legend: { display: false },
				  title: {
					display: true,
					text: 'Number of revisions for overall data for different user types',
					responsive: false,
					maintainAspectRatio: false
				  }
				}
			});
			
			new Chart(document.getElementById("doughnut-chart"), {
				type: 'doughnut',
				data: {
				  labels: ["Anononymous", "Bots", "Admin", "Regular"],
				  datasets: [
					{
					  label: "Number of revisions for overall data for different user types",
					  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9"],
					  data: [anon,bot,admin,reg]
					}
				  ]
				},
				options: {
				  title: {
					display: true,
					text: 'Number of revisions for overall data for different user types',
					responsive: false,
					maintainAspectRatio: false
				  }
				}
			});

           new Chart(document.getElementById("pie-chart"), {
         	    type: 'pie',
            	data: {
                labels: ["Anononymous", "Bots", "Admin", "Regular"],
                datasets: [
                    {
                        label: "Num  of revisions",
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9"],
                        data: [indanon,indbot,indadmin,indreg]
                    }
				]
            },
            options: {
                title: {
                    display: true,
                    text: 'Number of revisions per user type for selected article',
                    responsive: false,
                    maintainAspectRatio: false
                }
            }
        });
			
			new Chart(document.getElementById("bar-chart2"), {
				type: 'bar',
				data: {
				  labels: ["Anononymous", "Bots", "Admin", "Regular"],
				  datasets: [
					{
					  label: " Number of revisions per user type for Individual",
					  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
					  data: [indanon,indbot,indadmin,indreg]
					}
				  ]
				},
				options: {
				  legend: { display: false },
				  title: {
					display: true,
					text: 'Number of revisions per user type for Individual',
					responsive: false,
					maintainAspectRatio: false
				  }
				}
			});
	};
});

